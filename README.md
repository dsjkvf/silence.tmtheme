silence.tmTheme
===============


## About

This is a port of [Vim colorscheme `silence`](https://bitbucket.org/dsjkvf/vim-colors-silence) for Sublime Text as well as for the other software that uses this format (like [`bat`](https://github.com/sharkdp/bat), for example).

![screenshot](https://i.imgur.com/6TYhrLW.png)

## Install

From the menu, select `Preferences -> Browse Packages`. Then, clone this repository [and copy it] there. Restart Sublime Text and select `Preferences -> Color Scheme -> silence` from the menu.
